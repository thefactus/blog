class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title, presence: true, length: { minimum: 10, maximum: 255 }
  validates :body, presence: true

  acts_as_taggable

  belongs_to :user

end
