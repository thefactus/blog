Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  resources :posts do
    collection do
      get 'list', action: :index_admin
    end
  end

  get 'tags/:tag', to: 'posts#index', as: :tag

  get 'dashboard' => 'admin#dashboard'

  root 'home#index'
end
