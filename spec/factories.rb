FactoryGirl.define do
  factory :user do
    email 'johndoe@gmail.com'
    password '12345678'
  end

  factory :post do
    title 'HELLO WORLD'
    body 'BODY'
    association :user
  end
end
