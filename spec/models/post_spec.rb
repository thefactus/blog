require 'rails_helper'

describe Post do
  let(:post) { FactoryGirl.create :post }
  let(:user) { FactoryGirl.create :user }

  it 'is invalid' do
    post.title = nil
    post.body = nil
    expect(post).to_not be_valid
  end

  it 'requires title' do
    post.title = nil
    expect(post).to_not be_valid
    expect(post.errors[:title]).not_to be_blank
  end

  it 'requires body' do
    post.body = nil
    expect(post).to_not be_valid
    expect(post.errors[:body]).not_to be_blank
  end

  it 'title length should have at least 10 characters long' do
    post.title = 'TITLE'
    expect(post).to_not be_valid
    expect(post.errors.size).to eq(1)
  end

  it 'title length should be 255 characters max' do
    post.title = 'A' * 256
    expect(post).to_not be_valid
    expect(post.errors.size).to eq(1)
  end

  it 'belongs to a user' do
    expect(post).to be_valid
    expect(post.user.email).to eq('johndoe@gmail.com')
  end
end
