class AddUserIdToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts
    add_column :posts, :user_id, :integer
  end
end
